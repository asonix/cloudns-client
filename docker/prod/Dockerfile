ARG TARGET=aarch64-unknown-linux-musl
ARG ARCH_REPO=arm64v8

# build environment
FROM rustembedded/cross:$TARGET AS builder

ARG UID=991
ARG GID=991
ARG TARGET=aarch64-unknown-linux-musl
ARG TOOL=aarch64-linux-musl

ENV \
 TOOLCHAIN=stable

RUN \
 addgroup --gid "${GID}" build && \
 adduser \
    --disabled-password \
    --gecos "" \
    --ingroup build \
    --uid "${UID}" \
    --home /opt/build \
    build

ADD \
 https://sh.rustup.rs /opt/build/rustup.sh

RUN \
 chown -R build:build /opt/build

USER build
WORKDIR /opt/build

ENV \
 PATH=/opt/build/.cargo/bin:/usr/local/musl/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/sbin:/bin

RUN \
 chmod +x rustup.sh && \
 ./rustup.sh --default-toolchain $TOOLCHAIN --profile minimal -y && \
 rustup target add $TARGET

ARG TAG=main
ARG BINARY=cloudns-client
ARG PROJECT=cloudns-client
ARG GIT_REPOSITORY=https://git.asonix.dog/asonix/$PROJECT
ARG BUILD_MODE=release

ADD \
 --chown=build:build \
 $GIT_REPOSITORY/archive/$TAG.tar.gz \
 /opt/build/$TAG.tar.gz

USER build

RUN \
 tar zxf $TAG.tar.gz

WORKDIR /opt/build/$PROJECT

RUN \
 USER=build cargo build --target $TARGET --$BUILD_MODE && \
 $TOOL-strip target/$TARGET/$BUILD_MODE/$BINARY

# production environment
FROM $ARCH_REPO/alpine:3.14

ARG TARGET=aarch64-unknown-linux-musl
ARG UID=991
ARG GID=991
ARG BINARY=cloudns-client
ARG PROJECT=cloudns-client
ARG BUILD_MODE=release

ENV \
 BINARY=${BINARY}

RUN \
 addgroup -g "${GID}" app && \
 adduser -D -G app -u "${UID}" -g "" -h /opt/app app && \
 apk add tini

COPY \
 --from=builder \
 /opt/build/$PROJECT/target/$TARGET/$BUILD_MODE/$BINARY \
 /usr/local/bin/$BINARY

WORKDIR /opt/app
USER app
ENTRYPOINT ["/sbin/tini", "--"]
CMD /usr/local/bin/${BINARY}
