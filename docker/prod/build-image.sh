#!/usr/bin/env bash

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "deploy.sh"
    echo ""
    echo "Usage:"
    echo "	deploy.sh [repo] [tag] [arch]"
    echo ""
    echo "Args:"
    echo "	repo: The docker repository to publish the image"
    echo "	tag: The tag applied to the docker image"
    echo "	arch: The architecuture of the doker image"
}

function build() {
    REPO=$1
    ARCH=$2
    TAG=$3
    TARGET=$4
    TOOL=$5

    sudo docker build \
        --pull \
        --build-arg TAG=$TAG \
        --build-arg TARGET=$TARGET \
        --build-arg TOOL=$TOOL \
        --build-arg ARCH_REPO=$ARCH \
        -t $REPO:$ARCH-$TAG \
        -f Dockerfile \
        .
}

REPO=$1
TAG=$2
ARCH=$3

require "$REPO" "repo"
require "$TAG" "tag"
require "$ARCH" "arch"

case $ARCH in
    amd64)
        build \
            "$REPO" \
            "$ARCH" \
            "$TAG" \
            x86_64-unknown-linux-musl \
            x86_64-linux-musl
    ;;

    arm32v7)
        build \
            "$REPO" \
            "$ARCH" \
            "$TAG" \
            armv7-unknown-linux-musleabihf \
            arm-linux-musleabihf
    ;;

    arm64v8)
        build \
            "$REPO" \
            "$ARCH" \
            "$TAG" \
            aarch64-unknown-linux-musl \
            aarch64-linux-musl
    ;;

    *)
        echo "Invalid architecture provided"
        exit 1
    ;;
esac
