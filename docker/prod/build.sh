#!/usr/bin/env bash

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "build.sh"
    echo ""
    echo "Usage:"
    echo "	build.sh [tag] [branch] [push]"
    echo ""
    echo "Args:"
    echo "	tag: The git tag to be applied to the repository and docker build"
    echo "	branch: The git branch to use for tagging and publishing"
    echo "	push: Whether or not to push the image"
    echo ""
    echo "Examples:"
    echo "	./build.sh v0.3.0-alpha.13 main true"
    echo "	./build.sh v0.3.0-alpha.13-shell-out asonix/shell-out false"
}

function build_image() {
    tag=$1
    arch=$2
    push=$3

    ./build-image.sh asonix/cloudns-client $tag $arch

    sudo docker tag asonix/cloudns-client:$arch-$tag asonix/cloudns-client:$arch-latest

    if [ "$push" == "true" ]; then
        sudo docker push asonix/cloudns-client:$arch-$tag
        sudo docker push asonix/cloudns-client:$arch-latest
    fi
}

# Creating the new tag
new_tag="$1"
branch="$2"
push=$3

require "$new_tag" "tag"
require "$branch" "branch"
require "$push" "push"

if ! sudo docker run --rm -it arm64v8/alpine:3.11 /bin/sh -c 'echo "docker is configured correctly"'
then
    echo "docker is not configured to run on qemu-emulated architectures, fixing will require sudo"
    sudo docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

set -xe

sed -i "s/asonix\/cloudns-client:.*/asonix\/cloudns-client:$new_tag/" docker-compose.yml
git add docker-compose.yml

git commit -m "Version $new_tag"
git tag $new_tag

git push origin $new_tag
git push

build_image $new_tag arm64v8 $push
build_image $new_tag arm32v7 $push
build_image $new_tag amd64 $push

if [ "$push" == "true" ]; then
    ./manifest.sh cloudns-client $new_tag
    ./manifest.sh cloudns-client latest
fi
