#[derive(Debug)]
struct StatusError;

async fn fetch_ip(
    client: &reqwest::Client,
) -> Result<std::net::Ipv4Addr, Box<dyn std::error::Error>> {
    let ip_url = "https://icanhazip.com";

    let response = client.get(ip_url).send().await?;

    if !response.status().is_success() {
        return Err(StatusError.into());
    }

    let ip_str = response.text().await?;
    Ok(ip_str.trim().parse()?)
}

async fn update_dns(client: &reqwest::Client, url: &str) -> Result<(), Box<dyn std::error::Error>> {
    let response = client.get(url).send().await?;

    if !response.status().is_success() {
        return Err(StatusError.into());
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Hello, world!");
    let dyn_url = std::env::var("DYN_URL")?;

    let client = reqwest::Client::builder()
        .user_agent("Aode DNS Updater")
        .build()?;

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(15));

    let mut current_ip;
    let mut previous_ip = None;

    loop {
        interval.tick().await;
        match fetch_ip(&client).await {
            Ok(ip) => {
                current_ip = Some(ip);
                if current_ip != previous_ip {
                    println!(
                        "New IP {:?} updated from old ip {:?}",
                        current_ip, previous_ip
                    );
                    previous_ip = current_ip;

                    if let Err(e) = update_dns(&client, &dyn_url).await {
                        eprintln!("Failed to update dns: {}", e);
                    }
                }
            }
            Err(e) => {
                eprintln!("Failed to fetch current ip: {}", e);
            }
        }
    }
}

impl std::fmt::Display for StatusError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Invalid status code")
    }
}

impl std::error::Error for StatusError {}
